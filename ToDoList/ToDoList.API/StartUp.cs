﻿using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using ToDoList.bll;

[assembly: FunctionsStartup(typeof(ToDoList.API.StartUp))]
namespace ToDoList.API
{
    class StartUp : FunctionsStartup
    {
       

        public override void Configure(IFunctionsHostBuilder builder)
        {
            var connectionString = Environment.GetEnvironmentVariable("ToDoListDBConnection");
            builder.Services.AddDbContext<ToDoListDbContext>(
                options => options.UseSqlServer(
                    connectionString,
                    options => options.EnableRetryOnFailure())
                );
        }
    }
}
