using Microsoft.EntityFrameworkCore;
using NUnit.Framework;

namespace ToDoList.bll.test
{
    public class ToDoItemManagerTests
    {
        private DbContextOptions<ToDoListDbContext> inMemoryContextOptions =
            new DbContextOptionsBuilder<ToDoListDbContext>().UseInMemoryDatabase
            ("WorkshopToDoItems").Options;

        ToDoItemManager managerToTest;

        [SetUp]
        public void Setup()
        {
            var context = new ToDoListDbContext(inMemoryContextOptions);
            managerToTest = new ToDoItemManager(context);
        }

        [Test]
        public void AddItemTest()
        {
            //AAA structure: arrange, act, assert

            //set up (arrange)
            var addItemData = new AddItemDTO() { Description = "Test text." };

            //do (act)
            var addedItem = managerToTest.AddItem(addItemData);

            //check (assert)
            Assert.AreEqual(addItemData.Description, addedItem.Description);

        }

        [Test]
        public void UpdateItemTest()
        {
            //set up (arrange)
            var addItemData = new AddItemDTO() { Description = "Test text." };
            var addeditem = managerToTest.AddItem(addItemData);
            UpdateItemWithIdDTO updateData = new UpdateItemWithIdDTO()
            {
                newDescription = "New text.",
                NewState = ItemStates.Started
            };

            //do (act)
            var updatedItem = managerToTest.UpdateItemWithId(addeditem.ID, updateData);

            //check (assert)
            Assert.AreEqual(addeditem.ID,updatedItem.ID);
            Assert.AreEqual(updateData.newDescription, updatedItem.Description);
            Assert.AreEqual(updateData.NewState, updatedItem.State);
            Assert.AreEqual(addeditem.CreatedAt, updatedItem.CreatedAt);
        }
    }
}