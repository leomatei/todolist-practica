﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ToDoList.bll
{
    class Comment
    {
        [Key]
        public Guid ID { get; set; }

        public string CommentText { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
