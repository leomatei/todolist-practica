﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ToDoList.bll
{
    public class UpdateItemWithIdDTO
    {
        public string newDescription { get; set; }
        public ItemStates NewState { get; set; }
    }
}
